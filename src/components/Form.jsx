/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import ActiveUser from "./ActiveUser";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Input,
  Textarea,
  Divider,
} from "@chakra-ui/react";

// Firebase
import {
  collection,
  setDoc,
  doc,
  query,
  where,
  onSnapshot,
  updateDoc,
} from "firebase/firestore";
import { firestore } from "config/firebase";

export default function Form({ form, setForm }) {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const params = Object.fromEntries([...searchParams]);
  const typeInput = params.type;

  const nameUser = localStorage.getItem("name");

  useEffect(() => {
    if (!form.id) {
      navigate("/");
    }
  }, []);

  useEffect(() => {
    if (typeInput === "update") {
      const getData = async () => {
        const data = await query(
          collection(firestore, "items"),
          where("id", "==", form.id)
        );
        onSnapshot(data, (querySnapshot) => {
          querySnapshot.forEach((doc) => {
            setForm((current) => {
              const newData = doc.data();
              return {
                ...newData,
                name: compareInput(newData.name, current.name),
                qty: compareInput(newData.qty, current.qty),
                description: compareInput(
                  newData.description,
                  current.description
                ),
              };
            });
          });
        });
      };
      getData();
    }
  }, []);

  const getDataItemsUserActive = (id) => {
    return new Promise(async (resolve, reject) => {
      const data = await query(
        collection(firestore, "items_active_user"),
        where("id", "==", id)
      );
      onSnapshot(data, (querySnapshot) => {
        let dataDetail;
        querySnapshot.forEach((doc) => {
          dataDetail = doc.data();
        });
        resolve(dataDetail);
      });
    });
  };

  const handleSaveData = async () => {
    try {
      if (typeInput === "add") {
        const dataIRef = collection(firestore, "items");
        const dataIAURef = collection(firestore, "items_active_user");
        await setDoc(doc(dataIRef, `${form.id}`), form);
        await setDoc(doc(dataIAURef, `${form.id}`), {
          id: form.id,
          userActive: [],
        });
      } else {
        const docItemsRef = doc(firestore, "items", `${form.id}`);
        await updateDoc(docItemsRef, { ...form }, { merge: true });

        const itemActiveUserDetail = await getDataItemsUserActive(form.id);
        const userActive = itemActiveUserDetail?.userActive.filter(
          (item) => item !== nameUser
        );
        const docItemActiveUserDetail = doc(
          firestore,
          "items_active_user",
          `${form.id}`
        );
        await updateDoc(
          docItemActiveUserDetail,
          { id: form.id, userActive },
          { merge: true }
        );
      }
      alert(
        `Berhasil ${typeInput === "add" ? "menambahkan" : "merubah"} data !`
      );
      resetForm();
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  const handleCancel = async () => {
    try {
      if (typeInput === "update") {
        const itemActiveUserDetail = await getDataItemsUserActive(form.id);
        const userActive = itemActiveUserDetail?.userActive.filter(
          (item) => item !== nameUser
        );
        const docItemActiveUserDetail = doc(
          firestore,
          "items_active_user",
          `${form.id}`
        );
        await updateDoc(
          docItemActiveUserDetail,
          { id: form.id, userActive },
          { merge: true }
        );
      }
      resetForm();
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };

  const resetForm = () => {
    setForm({
      name: "",
      qty: "",
      description: "",
    });
  };

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setForm({
      ...form,
      [name]: value,
    });
  };

  const compareInput = (nextText, prevText) => {
    return nextText === prevText ? prevText : `${nextText}${prevText}`;
  };

  return (
    <>
      {typeInput === "update" && (
        <>
          <ActiveUser />
          <Divider my={2} />
        </>
      )}
      <Box>
        <FormControl mb={1}>
          <FormLabel>ID Barang</FormLabel>
          <Input type="text" size="sm" disabled value={form.id} />
        </FormControl>
        <FormControl mb={1}>
          <FormLabel>Nama Barang</FormLabel>
          <Input
            type="text"
            size="sm"
            placeholder="Input nama barang ..."
            name="name"
            value={form.name}
            onChange={(e) => handleChangeInput(e)}
          />
        </FormControl>
        <FormControl mb={1}>
          <FormLabel>Jumlah Barang</FormLabel>
          <Input
            type="text"
            size="sm"
            placeholder="Input jumlah barang ..."
            name="qty"
            value={form.qty}
            onChange={(e) => handleChangeInput(e)}
          />
        </FormControl>
        <FormControl mb={1}>
          <FormLabel>Deskripsi Barang</FormLabel>
          <Textarea
            placeholder="Input deskripsi barang ..."
            name="description"
            value={form.description}
            onChange={(e) => handleChangeInput(e)}
          />
        </FormControl>
        <Button
          colorScheme="teal"
          variant="solid"
          size="sm"
          my={2}
          width="100%"
          onClick={handleSaveData}
        >
          Simpan
        </Button>
        <Button
          colorScheme="teal"
          variant="outline"
          size="sm"
          width="100%"
          onClick={handleCancel}
        >
          Batal
        </Button>
      </Box>
    </>
  );
}
