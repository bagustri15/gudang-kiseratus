import React from "react";
import ModalAuth from "./ModalAuth";
import { Box, Button, Heading, Avatar, Divider } from "@chakra-ui/react";

export default function Header({ isOpen, onOpen, onClose }) {
  const nameUser = localStorage.getItem("name");

  const handleLogout = () => {
    localStorage.removeItem("name");
    onOpen();
  };

  return (
    <>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        my={5}
      >
        <Heading as="h2" size="lg">
          Gudang Kiseratus 🔥
        </Heading>
        <Box>
          {localStorage.getItem("name") && (
            <>
              <Avatar
                name={nameUser || "Anonymous"}
                src={`https://ui-avatars.com/api/?name=${
                  nameUser || "Anonymous"
                }`}
                size="sm"
                mr={2}
              />
              <Button
                colorScheme="teal"
                variant="solid"
                size="sm"
                onClick={handleLogout}
              >
                Logout
              </Button>
            </>
          )}
        </Box>
        <ModalAuth isOpen={isOpen} onClose={onClose} />
      </Box>

      <Divider my={2} />
    </>
  );
}
