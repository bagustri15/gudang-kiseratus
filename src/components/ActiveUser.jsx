/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import { Stack, Avatar, AvatarBadge } from "@chakra-ui/react";

// Firebase
import { collection, query, where, onSnapshot } from "firebase/firestore";
import { firestore } from "config/firebase";

export default function ActiveUser() {
  const [searchParams] = useSearchParams();
  const params = Object.fromEntries([...searchParams]);
  const typeInput = params.type;
  const itemId = params.id;

  const [activeUser, setActiveUser] = useState([]);

  useEffect(() => {
    if (typeInput === "update") {
      const getData = async () => {
        const data = await query(
          collection(firestore, "items_active_user"),
          where("id", "==", +itemId)
        );
        onSnapshot(data, (querySnapshot) => {
          querySnapshot.forEach((doc) => {
            setActiveUser(doc.data().userActive);
          });
        });
      };
      getData();
    }
  }, []);

  return (
    <Stack direction="row" spacing={3} justifyContent="center">
      {activeUser.map((item, index) => (
        <Avatar
          name={item || "Anonymous"}
          src={`https://ui-avatars.com/api/?name=${item || "Anonymous"}`}
          size="sm"
          key={index}
        >
          <AvatarBadge boxSize="1.25em" bg="teal" />
        </Avatar>
      ))}
    </Stack>
  );
}
