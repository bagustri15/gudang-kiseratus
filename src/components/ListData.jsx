/* eslint-disable react-hooks/exhaustive-deps */
import { useNavigate } from "react-router-dom";
import {
  Button,
  ButtonGroup,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from "@chakra-ui/react";
import { EditIcon, DeleteIcon } from "@chakra-ui/icons";
import { randomFixedInteger } from "helper";

// Firebase
import {
  collection,
  doc,
  query,
  where,
  onSnapshot,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";
import { firestore } from "config/firebase";

export default function ListData({ data, form, setForm }) {
  const navigate = useNavigate();
  const nameUser = localStorage.getItem("name");

  const handleUpdateData = async (data) => {
    try {
      const { id } = data;
      const itemDetail = await getDataItems(id);
      setForm(itemDetail);
      const itemActiveUserDetail = await getDataItemsUserActive(id);
      const setUserActive = {
        userActive: [...itemActiveUserDetail?.userActive, nameUser],
      };
      const docRef = doc(firestore, "items_active_user", `${id}`);
      await updateDoc(docRef, setUserActive, { merge: true });
      navigate(`/?type=update&id=${id}`);
    } catch (error) {
      console.log(error);
    }
  };

  const getDataItems = (id) => {
    return new Promise(async (resolve, reject) => {
      const data = await query(
        collection(firestore, "items"),
        where("id", "==", id)
      );
      onSnapshot(data, (querySnapshot) => {
        let dataDetail;
        querySnapshot.forEach((doc) => {
          dataDetail = doc.data();
        });
        resolve(dataDetail);
      });
    });
  };

  const getDataItemsUserActive = (id) => {
    return new Promise(async (resolve, reject) => {
      const data = await query(
        collection(firestore, "items_active_user"),
        where("id", "==", id)
      );
      onSnapshot(data, (querySnapshot) => {
        let dataDetail;
        querySnapshot.forEach((doc) => {
          dataDetail = doc.data();
        });
        resolve(dataDetail);
      });
    });
  };

  const handleDeleteData = async (id) => {
    try {
      const confirmDelete = window.confirm("Hapus barang ini ?");
      if (confirmDelete) {
        const docRef = doc(firestore, "items", `${id}`);
        await deleteDoc(docRef).then(() => {
          alert("Berhasil menghapus data !");
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Button
        colorScheme="teal"
        variant="solid"
        size="sm"
        my={2}
        width="100%"
        onClick={() => {
          setForm({
            id: randomFixedInteger(3),
            ...form,
          });
          navigate(`/?type=add`);
        }}
      >
        Tambah Barang
      </Button>
      <TableContainer>
        <Table variant="simple" colorScheme="teal">
          <TableCaption>List Barang Kiseratus ~</TableCaption>
          <Thead>
            <Tr>
              <Th>ID</Th>
              <Th>Nama</Th>
              <Th>Jumlah</Th>
              <Th>Deskripsi</Th>
              <Th>Aksi</Th>
            </Tr>
          </Thead>
          <Tbody>
            {data.length > 0 ? (
              data.map((item) => (
                <Tr key={item.id}>
                  <Td>{item.id}</Td>
                  <Td>{item.name || "-"}</Td>
                  <Td>{item.qty || "-"}</Td>
                  <Td>{item.description || "-"}</Td>
                  <Td>
                    <ButtonGroup variant="outline" spacing="0.1">
                      <Button
                        colorScheme="teal"
                        variant="ghost"
                        size="sm"
                        onClick={() => handleUpdateData(item)}
                      >
                        <EditIcon />
                      </Button>
                      <Button
                        colorScheme="teal"
                        variant="ghost"
                        size="sm"
                        onClick={() => handleDeleteData(item.id)}
                      >
                        <DeleteIcon />
                      </Button>
                    </ButtonGroup>
                  </Td>
                </Tr>
              ))
            ) : (
              <Tr>
                <Td colSpan={5} textAlign="center">
                  Data Tidak Ditemukan
                </Td>
              </Tr>
            )}
          </Tbody>
          <Tfoot>
            <Tr>
              <Th>ID</Th>
              <Th>Nama</Th>
              <Th>Jumlah</Th>
              <Th>Deskripsi</Th>
              <Th>Aksi</Th>
            </Tr>
          </Tfoot>
        </Table>
      </TableContainer>
    </>
  );
}
