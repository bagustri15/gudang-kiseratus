import React, { useState } from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Input,
  Button,
} from "@chakra-ui/react";

export default function ModalAuth({ isOpen, onClose }) {
  const [name, setName] = useState(localStorage.getItem("name") || "");
  const handleSaveName = () => {
    localStorage.setItem("name", name);
    setName("");
    onClose();
  };
  return (
    <>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        closeOnEsc={false}
        closeOnOverlayClick={false}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Set User 🤠</ModalHeader>
          <ModalBody>
            <Input
              autoFocus
              placeholder="Input your name ..."
              maxLength={50}
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </ModalBody>

          <ModalFooter gap={2}>
            <Button
              colorScheme="teal"
              variant="outline"
              isDisabled={!name}
              onClick={() => setName("")}
            >
              Reset
            </Button>
            <Button
              colorScheme="teal"
              variant="solid"
              isDisabled={!name}
              onClick={handleSaveName}
            >
              Save
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
