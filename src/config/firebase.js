import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBH02ivOUQGAfhxgztpyHSEbQGDHmNQ3nY",
  authDomain: "gudang-kiseratus.firebaseapp.com",
  projectId: "gudang-kiseratus",
  storageBucket: "gudang-kiseratus.appspot.com",
  messagingSenderId: "308336534593",
  appId: "1:308336534593:web:384897759d6bf6cd7792c0",
};

const app = initializeApp(firebaseConfig);
export const firestore = getFirestore(app);
