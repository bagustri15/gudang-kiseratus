const dataBarang = [
  {
    id: 123,
    name: "Kursi",
    qty: 20,
    description: "Kursi Hitam",
  },
  {
    id: 456,
    name: "Meja",
    qty: 20,
    description: "Meja Kayu",
  },
];
export default dataBarang;
