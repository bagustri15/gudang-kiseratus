export const actionTypes = {
  ADD_ITEM: "ADD_ITEM",
  UPDATE_ITEM: "UPDATE_ITEM",
  DELETE_ITEM: "DELETE_ITEM",
  RESET_ITEM: "RESET_ITEM",
};

export const addItem = () => {
  return function (dispatch) {
    dispatch({
      type: actionTypes.ADD_ITEM,
    });
  };
};
export const updateItem = () => {
  return function (dispatch) {
    dispatch({
      type: actionTypes.UPDATE_ITEM,
    });
  };
};
export const deleteItem = () => {
  return function (dispatch) {
    dispatch({
      type: actionTypes.DELETE_ITEM,
    });
  };
};
export const resetItem = () => {
  return function (dispatch) {
    dispatch({
      type: actionTypes.RESET_ITEM,
    });
  };
};
