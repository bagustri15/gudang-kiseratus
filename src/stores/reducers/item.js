import { actionTypes } from "../actions/item";

const initialState = {
  form: {
    name: "",
    qty: "",
    description: "",
  },
};

const itemReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.RESET_ITEM: {
      return {
        ...state,
        form: {
          name: "",
          qty: "",
          description: "",
        },
      };
    }
    default: {
      return state;
    }
  }
};

export default itemReducer;
