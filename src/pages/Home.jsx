/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useDisclosure, Container } from "@chakra-ui/react";
import imgNoInternet from "../assets/no_internet.gif";

// Firebase
import { collection, query, onSnapshot } from "firebase/firestore";
import { firestore } from "config/firebase";

import Header from "components/Header";
import ListData from "components/ListData";
import Form from "components/Form";

export default function Home({ isOnline }) {
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const params = Object.fromEntries([...searchParams]);
  const typeInput = params.type?.toLocaleLowerCase();
  const idBarang = params.id;

  const nameUser = localStorage.getItem("name");

  const [data, setData] = useState([]);
  const [form, setForm] = useState({
    name: "",
    qty: "",
    description: "",
  });
  const { isOpen, onOpen, onClose } = useDisclosure();

  useEffect(() => {
    if (typeInput && !["add", "update"].includes(typeInput)) {
      alert("URL tidak ditemukan !");
      return navigate("/");
    }
    if (!data || !nameUser) {
      return navigate("/");
    }
    if (typeInput === "add" && idBarang) {
      return navigate("?type=add");
    }
    if (typeInput === "update" && !idBarang) {
      return navigate("/");
    }
  }, [typeInput]);

  useEffect(() => {
    if (!nameUser) {
      onOpen();
    }
  }, [nameUser, onOpen]);

  useEffect(() => {
    handleGetData();
    return () => {
      setData([]);
    };
  }, []);

  const handleGetData = async () => {
    try {
      // Save Firebase
      const data = await query(collection(firestore, "items"));
      await onSnapshot(data, (querySnapshot) => {
        const listData = [];
        querySnapshot.forEach((doc) => {
          listData.push(doc.data());
        });
        setData(listData);
      });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Container maxW="container.sm">
      {isOnline ? (
        <>
          {/* Header */}
          <Header isOpen={isOpen} onOpen={onOpen} onClose={onClose} />
          {/* End Header */}
          {/* Form */}
          {typeInput && <Form form={form} setForm={setForm} />}
          {/* End Form */}
          {/* Table */}
          {!typeInput && <ListData data={data} form={form} setForm={setForm} />}
          {/* End Table */}
        </>
      ) : (
        <>
          <img src={imgNoInternet} alt="no-internet" />
        </>
      )}
    </Container>
  );
}
